﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RandomPositionSpawner : MonoBehaviour {
    public GameObject spawnedObject;  

    private BoxCollider2D spawnableRadius;

    private void Start()
    {
        spawnableRadius = GetComponent<BoxCollider2D>();  
    }

    public void spawnObject()
    {
        float boxWidth = spawnableRadius.bounds.extents.x; 
        float boxHeight = spawnableRadius.bounds.extents.y;

        Vector2 randomPoint = new Vector2(Random.Range(-boxWidth, boxWidth), Random.Range(-boxHeight, boxHeight));
        randomPoint += (Vector2)transform.position;

        Instantiate(spawnedObject, randomPoint, Quaternion.identity);  
    }
}
