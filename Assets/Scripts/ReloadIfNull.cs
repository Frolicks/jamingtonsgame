﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ReloadIfNull : MonoBehaviour {
    public GameObject[] trackedObjects; 
    public float reloadTime; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        foreach(GameObject go in trackedObjects)
        {
            if(!go || go.CompareTag("Player") && !go.GetComponent<TriangleController>().alive)
            {
                Invoke("reload", reloadTime); 
            }
        }
		
	}

    public void reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
