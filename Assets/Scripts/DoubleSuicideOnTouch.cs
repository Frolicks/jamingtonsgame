﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleSuicideOnTouch : MonoBehaviour {
    public GameplayController gameplayController;

    private void Start()
    {
        gameplayController = GameObject.Find("GameController").GetComponent<GameplayController>(); 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        gameplayController.GetComponent<AudioSource>().Play();

        if (gameplayController)
        {
            gameplayController.incrementScore();
        }


        if (collision.gameObject.GetComponent<Circle>())
        {
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<TriangleController>().die(); 
        }

        Destroy(gameObject);
    }
}
