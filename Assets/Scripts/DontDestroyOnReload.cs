﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class DontDestroyOnReload : MonoBehaviour {
    public int reloadingScene; 

	// Use this for initialization
	void Start () {
        if(FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject); 
        }

        DontDestroyOnLoad(gameObject);
        reloadingScene = SceneManager.GetActiveScene().buildIndex; 



    }
	
	// Update is called once per frame
	void Update () {
		if(SceneManager.GetActiveScene().buildIndex != reloadingScene)
        {
            Destroy(gameObject); 
        }
	}
}
