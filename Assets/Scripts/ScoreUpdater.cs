﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class ScoreUpdater : MonoBehaviour {

    private GameObject gameController;

    private void Start()
    {
        gameController = GameObject.Find("GameController"); 
    }

    private void FixedUpdate()
    {
        GetComponent<Text>().text = gameController.GetComponent<GameplayController>().score.ToString(); 
    }


}
