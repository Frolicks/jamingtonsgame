﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEarlyIfNoHazards : MonoBehaviour
{
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        bool presentObject = GameObject.FindObjectOfType<Circle>(); 

        if(!presentObject)
        {
            GetComponent<ShootRegularly>().shoot(); 
        }

            
	}
}
