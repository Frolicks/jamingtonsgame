﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredSideCollision : MonoBehaviour {

    public Circle.color color;

    private TriangleController triangleController;  

	// Use this for initialization
	void Start () {
        triangleController = GetComponentInParent<TriangleController>();  
	}
	
	// Update is called once per frame
	void Update () {
		

	}

    public bool processColorCombination(Circle collidedCircle)
    {
        if (collidedCircle.gameObject.tag != color.ToString())
        {
            //triangleController.touchedWrongColor = true;
            return false;
        }
        else
        {
            //triangleController.touchedCorrectColor = true;

            return true;
        }
    }
}
