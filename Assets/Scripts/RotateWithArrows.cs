﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWithArrows : MonoBehaviour {
    public float rotationSpeed;

    private float arrowAxis; 

    private void Update()
    {
        arrowAxis = Input.GetAxisRaw("ArrowHorizontal");
    }

    private void FixedUpdate()
    {
        if(GetComponent<TriangleController>().alive)
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + arrowAxis * -Vector3.forward * rotationSpeed * Time.deltaTime); 
    }
}
