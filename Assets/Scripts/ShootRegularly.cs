﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootRegularly : MonoBehaviour {
    public GameObject shotGameObject;
    public float shotForce;

    public float shootRate;
    private float shootCoolDown;  

	// Use this for initialization
	void Start () {
        shoot();
	}
	
	// Update is called once per frame
	void Update () {
        shootCoolDown -= Time.deltaTime;  

        if(shootCoolDown <= 0)
        {
            shoot(); 
        }
	}

    public void shoot()
    {
        GameObject shot = Instantiate(shotGameObject, transform.position, Quaternion.identity);
        shot.GetComponent<Rigidbody2D>().AddForce(transform.up * shotForce);
        shootCoolDown = shootRate; 
    }
}
