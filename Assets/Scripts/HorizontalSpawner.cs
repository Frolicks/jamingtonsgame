﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; 

public class HorizontalSpawner : MonoBehaviour {
    public GameObject spawnedObject;  

    public float spawnRate;

    public int minWaveDensity, maxWaveDensity;

    public float spawnWidth;  
    
	// Use this for initialization
	void Start () {
        StartCoroutine(spawnWaves());
    }
	
	IEnumerator spawnWaves()
    {
        while (true)
        {
            spawnWave();
            yield return new WaitForSeconds(spawnRate);
        }

    }
    
    private void spawnWave()
    {
        int waveDensity = Random.Range(minWaveDensity, maxWaveDensity); 

        for (int i = 0; i < waveDensity; i++)
        {
            Instantiate(spawnedObject, transform.position + Vector3.right * Random.Range(-spawnWidth, spawnWidth) + Vector3.up * Random.Range(0, 3f), Quaternion.identity); 
        }
    }

    public void stopSpawning()
    {
        StopCoroutine(spawnWaves()); 
    }
}
