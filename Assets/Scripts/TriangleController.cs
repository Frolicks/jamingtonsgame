﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class TriangleController : MonoBehaviour {

    public bool touchedWrongColor, touchedCorrectColor;
    public bool alive; 

	// Use this for initialization
	void Start () {
        alive = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void die()
    {
        alive = false;

        GameObject spriteGO = transform.Find("Sprite").gameObject;
        spriteGO.GetComponent<SpriteRenderer>().enabled = false;
        spriteGO.GetComponent<ParticleSystem>().Play();

        Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
        foreach (Collider2D collider in colliders)
        {
            collider.isTrigger = true;
        }

        GetComponent<AudioSource>().Play();


        GameObject.Find("GameController").GetComponent<GameplayController>().lose();
    }

    private void LateUpdate()
    {
        if(alive)
        {
            if (touchedWrongColor && !touchedCorrectColor)
            {
                die(); 
            }
            else
            {
                touchedWrongColor = false;
                touchedCorrectColor = false;    
            }
        }

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Circle circle = collision.gameObject.GetComponent<Circle>(); 
        if(circle && !circle.safe)
        {
            die(); 
        }
    }
}
