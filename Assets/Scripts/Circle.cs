﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour {

    public enum color { Red, Green, Blue};

    public color circleColor;

    private SpriteRenderer spriteRenderer;

    public bool safe;  


	// Use this for initialization
	void Start () {
        safe = false; 

        spriteRenderer = GetComponent<SpriteRenderer>();
        circleColor = (color)(Random.Range(0, 3));
        
        switch (circleColor)
        {
            case color.Red:
                spriteRenderer.color = Color.red;
                gameObject.tag = "Red";  
                break;
            case color.Green:
                spriteRenderer.color = Color.green;
                gameObject.tag = "Green";  
                break;
            case color.Blue:
                spriteRenderer.color = Color.blue;
                gameObject.tag = "Blue";  
                break;  
        }
		

	}

    /* private void OnTriggerStay2D(Collider2D collision)
    {
        ColoredSideCollision coloredSide = collision.GetComponent<ColoredSideCollision>(); 
        if (coloredSide)
        {
            coloredSide.processColorCombination(this); 
        }
    } */

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ColoredSideCollision coloredSide = collision.GetComponent<ColoredSideCollision>();
        if (coloredSide && coloredSide.processColorCombination(this))
        {
            safe = true;  
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        safe = false; 
    }





    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<PlayRandomAudioClip>().PlayRandomSound();

        if (collision.relativeVelocity.magnitude > 5 && GameObject.Find("Ball Cannon")) // ball cannon present
        {
            Camera.main.GetComponent<Shake>().shakeForSeconds(0.05f, 0.08f); 
        } 
    }
}
