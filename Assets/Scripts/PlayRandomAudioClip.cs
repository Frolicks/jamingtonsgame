﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomAudioClip : MonoBehaviour {
    public AudioClip[] audioClips;

    public float volume;  

    public void PlayRandomSound()
    {
        AudioClip randSound = audioClips[(Random.Range(0, audioClips.Length))];

        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = randSound;
        audio.PlayOneShot(audio.clip, volume);
    }
}
