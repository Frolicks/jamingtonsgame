﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayController : MonoBehaviour {

    public int score;


    public GameObject canvas; 

    


	// Use this for initialization
	void Start () {
        score = 0; 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void lose()
    {
        if(score > PlayerPrefs.GetInt("High Score"))
        {
            PlayerPrefs.SetInt("High Score", score); 
        }

        canvas.SetActive(true); 
    }

    public void incrementScore()
    {
        score++;
        GetComponent<RandomPositionSpawner>().spawnObject();  
    }
}
