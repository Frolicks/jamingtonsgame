﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndDash : MonoBehaviour {

    public float speed, dashSpeed;
    public float dashTime;

    public bool dashing; 

    private Rigidbody2D rb;
    

    private Vector2 playerInput;  

	// Use this for initialization
	void Awake () {
        rb = GetComponent<Rigidbody2D>();
        dashing = false;  
	}

    private void Update()
    {
        if (GetComponent<TriangleController>().alive)
        {
            playerInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            if (Input.GetKeyDown(KeyCode.Space) && !dashing)
            {
                StartCoroutine(dashTowards(playerInput));
            }
        }
  
    }

    private void FixedUpdate()
    {
        if(!dashing)
        {
            moveInDirection(playerInput, speed);
        }
        
        

    }

    private IEnumerator dashTowards(Vector2 direction)
    {
        GetComponent<PlayRandomAudioClip>().PlayRandomSound(); 

        dashing = true;
        moveInDirection(direction, dashSpeed);

        yield return new WaitForSeconds(dashTime);

        dashing = false;  
       
    }

    public void moveInDirection(Vector2 direction, float moveSpeed) 
    {
        rb.velocity = direction * moveSpeed;
    }
}

