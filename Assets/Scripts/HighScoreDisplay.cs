﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class HighScoreDisplay : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = "High Score: " + PlayerPrefs.GetInt("High Score");
        
	}
}
